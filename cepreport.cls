\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cepreport}

\newcommand\mtfntsize{12pt}

\LoadClass[\mtfntsize,letterpaper]{article}
\RequirePackage[margin=3cm]{geometry}
\RequirePackage{fontawesome}
\RequirePackage{graphicx}
\RequirePackage{hyperref, url}
\RequirePackage{array}
\RequirePackage{multirow}
\RequirePackage{xcolor,colortbl}
\RequirePackage{multirow, makecell, hhline}
\RequirePackage{cellspace}
\RequirePackage{tabularx}
\RequirePackage{longtable}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\RequirePackage[section]{placeins}

\setlength{\parindent}{0pt}
\pagestyle{fancy}


%% Localisation
\newif\iffr
\newif\ifen
\newcommand{\en}[1]{\ifen#1\fi}
\newcommand{\fr}[1]{\iffr#1\fi}

%% Title
\newcommand\mDocumentTitle{}
\newcommand\mDocumentNumber{}
\newcommand\mProjectName{}
\newcommand\mProjectNumber{}
\newcommand\mRevisionNumber{}
\newcommand\mRevisionDescription{}
\newcommand\mAcceptationPerson{}
\newcommand\mAcceptationDate{}
\newcommand\mApprobationPerson{}
\newcommand\mApprobationDate{}

\newcommand{\DocumentTitle}[1]{\renewcommand\mDocumentTitle{#1}}
\newcommand{\DocumentNumber}[1]{\renewcommand\mDocumentNumber{#1}}
\newcommand{\ProjectName}[1]{\renewcommand\mProjectName{#1}}
\newcommand{\ProjectNumber}[1]{\renewcommand\mProjectNumber{#1}}
\newcommand{\RevisionNumber}[1]{\renewcommand\mRevisionNumber{#1}}
\newcommand{\RevisionDescription}[1]{\renewcommand\mRevisionDescription{#1}}
\newcommand{\AcceptationPerson}[1]{\renewcommand\mAcceptationPerson{#1}}
\newcommand{\AcceptationDate}[1]{\renewcommand\mAcceptationDate{#1}}
\newcommand{\ApprobationPerson}[1]{\renewcommand\mApprobationPerson{#1}}
\newcommand{\ApprobationDate}[1]{\renewcommand\mApprobationDate{#1}}
